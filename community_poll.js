Drupal.behaviors.community_poll = function () {
  // Add delete buttons for choices
  $('td.community-poll-delete').append('<div title="Odstranit možnost">&times;</div>');
  $('td.community-poll-delete:last div').remove();
  $('.community-poll-delete div').click(function () {
    $(this).parent().parent().fadeOut(function () {
      $(this).children('td:eq(1)').children('div').children('input').val('');
    });
  });
  
  // Add votes buttons for choices
  $('td.community-poll-votes').append('<div title="Ukázat hlasy"></div>');
  $('#community_poll_choices_form td .votes').hide();
  $('td.community-poll-votes:last div').remove();
  $('.community-poll-votes div').click(function () {
    $(this).parent().parent().children('td:eq(1)').children('.votes').slideToggle();
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
    
  });
};